/**
 * Created by wojtekfratczak on 27.04.2016.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PredictionsMain {

    public static void main(String[] args) {
        try (BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in))) {

            System.out.println("Podaj swoje imię i nazwisko: ");
            String fullName = bufferRead.readLine();
            PredictionsShower shower = new PredictionsShower();
            shower.fullName = fullName;
            shower.showHealthPrediction();
            shower.showLovePrediction();
            shower.showWorkPrediction();

        } catch(IOException ex) {

        }
    }

}
