/**
 * Created by wojtekfratczak on 27.04.2016.
 */

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;

import java.lang.reflect.Array;

public class PredictionsTest {

    @Test
    public void numberOfLovePredictions() {
        assertThat("MILOSC predictions not equal 12", Predictions.MILOSC.length == 12);
    }

    @Test
    public void numberOfWorkPredictions() {
        assertThat("PRACA predictions not equal 12", Predictions.PRACA.length == 12);
    }

    @Test
    public void numberOfHealthPredictions() {
        assertThat("ZDROWIE predictions not equal 12", Predictions.ZDROWIE.length == 12);
    }



    @Test
    public void emptyName() {
        PredictionsShower shower = new PredictionsShower();
        shower.fullName = "";
        int number = shower.healthPredictionNumber();
        assertThat("", number <= 12 && number >= 0);
    }

    @Test
    public void healthPredictionNumber() {
        PredictionsShower shower = new PredictionsShower();
        shower.fullName = "Daniel Budyński";
        int number = shower.healthPredictionNumber();
        assertThat(number, is(equalTo(3)));
        assertThat("", number <= 12 && number >= 0);
    }

    @Test
    public void lovePredictionNumber() {
        PredictionsShower shower = new PredictionsShower();
        shower.fullName = "Wojciech Frątczak";
        int number = shower.lovePredictionNumber();
        assertThat("", number <= 12 && number >= 0);
    }

    @Test
    public void workPredictionNumber() {
        PredictionsShower shower = new PredictionsShower();
        shower.fullName = "Daniel Budyński";
        int number = shower.workPredicitionNumber();
        assertThat("", number <= 12 && number >= 0);
    }

}
