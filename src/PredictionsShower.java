/**
 * Created by wojtekfratczak on 27.04.2016.
 */

import java.util.Calendar;
import java.util.Random;

public class PredictionsShower {

    public String fullName = "";

    public void showLovePrediction() {
        int predictionNumber = this.lovePredictionNumber();
        String prediction = Predictions.MILOSC[predictionNumber];
        System.out.println("Przepowiednia w miłości: " + prediction);
    }

    public int lovePredictionNumber() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DAY_OF_MONTH - 1) % 12;
    }

    public void showHealthPrediction() {
        int predictionNumber = this.healthPredictionNumber();
        String prediction = Predictions.ZDROWIE[predictionNumber];
        System.out.println("Przepowiednia w zdrowiu: " +prediction);
    }

    public int healthPredictionNumber() {
        return this.fullName.length() % 12;
    }

    public void showWorkPrediction() {
        int predictionNumber = this.workPredicitionNumber();
        String prediction = Predictions.PRACA[predictionNumber];
        System.out.println("Przepowiednia w pracy: " +prediction);
    }

    public int workPredicitionNumber() {
        Random generator = new Random();
        return generator.nextInt(12) + 1;
    }

}
